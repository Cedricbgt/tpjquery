/* Mise en forme du document Langages.html entièrement en jQuery */

var fonce = '#666699'; // couleur foncée pour fond H1 et bordures
var clair = '#EEEEFF'; // couleur claire pour fond des div et de l'élément actif
var bordurePleine = "thin solid " + fonce;
var bordureVide = "thin solid " + clair;

var actif = 0; // Indice du div actif, utile pour la liste des éléments de menu et la liste des div
               // de classe "main". Les deux listes sont supposées dans le même ordre

// Mise en forme des titres
//    - h1 en blanc sur foncé, centré
//    - h2 en foncé, bordure en bas foncée
function titres() {
    $('h1').css({"text-align":"center","color":"white","background-color":fonce});
    $('h2').css({"border-bottom":bordurePleine,"color":fonce});
}

// Initialisation des div de présentation d'un langage
//    - intialement tous cachés
//    - bordure foncée et fond clair
//    - écartement entre la bordure et le texte de 5 pixels en haut et en bas, de 10 pixels à droite et à gauche
function lesDiv() {
    $("div.main").hide();
    $("div.main").css({"border" : bordurePleine, "background-color" : clair, "padding" : "5px 5px 10px 10px" });
}

// Rafraîchir l'affichage suite à un clic sur un onglet
//   - masquer le div actif et afficher le nouveau
//   - rafraîchir les propriétés du menu :
//      -- actif passe en passif (bordure du bas et fond)
//      -- nouveau passe en actif (bordure du bas et fond)
//      -- actif = nouveau
function rafraichir(nouveau) {
    $('div.main').eq(actif).css({"display": "inline-block", "margin-top": "-1px"}).hide();
    $('div.main').eq(nouveau).css({"display": "inline-block"}).show();
    $("#sommaire li").eq(actif).css({ "background-color" : "white"});
    $("#sommaire li").eq(nouveau).css({"border-bottom": "1px solid " +clair, "background-color" : clair});
    actif = nouveau;
    
}

// Mise en forme du menu
//    - forcer la largeur et centrer le div
//    - texte en gras et foncé 
//    - pas de puce pour ul, position relative décalée de 1 pixel vers le bas
//    - li en mode block, flottants à gauche, bordure foncée, marge de 5 pixels à droite et à gauche
//    - effacer les liens, ne laisser que le texte
//    - ajouter une action pour le clic : rafraichir avec l'indice du li dans la liste des li.
function menu() {
    $("#sommaire").css({"margin" : "0px auto","font-weight" : "bold", "color" : fonce, "width" : "300px"});
    $("#sommaire ul").css({"list-style" : "none", "margin-top" : "1px", "position":"relative"});
    $("#sommaire li").css({"display" : "block", "float": "left", "border" : fonce, "margin-left" : "5px", "margin-right" : "5px","padding" : "1px 1px 5px 5px", "border-left-style":"solid","border-right-style":"solid","border-top-style":"solid","border-width": "thin" });
    $("#sommaire a").attr("href",null);
    $("#sommaire li").click(function(){
        rafraichir($(this).index());
    });
}

// Mise en forme des liens "Pour en savoir plus"
//    - paragraphe aligné à droite
//    - lien en couleur foncée passant en blanc sur foncé au passage de la souris (voir hover)
function liensPlus() {
    $('p').css({"text-align" : "right"});
    $('p a').mouseenter(function(){
        $(this).css("color","white");
    });
     $('p a').mouseout(function(){
        $(this).css("color","blue");
    });
}

// Installation d'un lien dans le titre de la section
// Insérer un lien vers le site cité dans le paragraphe <p class="plus">)
// Ainsi "Le langage Python présenté par..." doit devenir "Le langage <a href="http://...">Python</a> présenté par..."
// Il faut récupérer le mot-clé associé à chaque section dans le lien <a name="mot-clé">
// Il faut cloner le lien "Pour en savoir plus" et changer le texte
// Finalement il faut reconstruire le texte du h2 (titre de section) en ajoutant le lien
function lienTitres() {     
    
}

$(document).ready(function () {
    $('body').css('font-family', "Verdana, Arial, sans-serif");
    titres();
    lesDiv();
    menu();
    liensPlus();
    lienTitres();
    rafraichir(0);
});
